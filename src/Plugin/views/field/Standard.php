<?php

namespace Drupal\mongodb\Plugin\views\field;

use Drupal\views\Plugin\views\field\Standard as CoreStandard;

/**
 * Overriding the views field plugin "standard".
 */
class Standard extends CoreStandard {

}
