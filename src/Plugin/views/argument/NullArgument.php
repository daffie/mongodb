<?php

namespace Drupal\mongodb\Plugin\views\argument;

use Drupal\views\Plugin\views\argument\NullArgument as CoreNullArgument;

/**
 * Overriding the views field plugin "null".
 */
class NullArgument extends CoreNullArgument {}
