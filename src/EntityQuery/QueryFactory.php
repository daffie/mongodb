<?php

namespace Drupal\mongodb\EntityQuery;

use Drupal\Core\Entity\Query\Sql\QueryFactory as BaseQueryFactory;

/**
 * MongoDB specific entity query implementation.
 */
class QueryFactory extends BaseQueryFactory {

}
